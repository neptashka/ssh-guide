# Guide how to work with SSH keys
## Working with a single account
1. Create a key for each account and specify your email after -c. "key_name" - name to identify this key. 
These keys will be visible in your user system folder in .ssh directory
```
ssh-keygen -t ed25519 -f ~/.ssh/key_name -C "name@example.com"
```

2. Copy public keys manually or with following commands:
```
#macOS
tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy
```
```
#Linux
xclip -sel clip < ~/.ssh/id_ed25519.pub
```
```
#Windows (GitBush)
cat ~/.ssh/id_ed25519.pub | clip
```

3. Go to your account in GitLab User->Profile->SSH keys and paste this key. 

4. Add the key to ssh-agent (optional). With this command you don't have to add passphrase every time you push or pull changes.
```
ssh-add ~/.ssh/<private-key>
```
In case you need to delete it:
```
ssh-add -D
```

5. Verify that you can connect (optional)
```
ssh -T git@gitlab.com
```

## Working with several accounts
[Here](https://medium.com/uncaught-exception/setting-up-multiple-gitlab-accounts-82b70e88c437#:~:text=GitLab%20does%20not%20allow%20you,~%2F.) you can find a detailed explanation

1. Create config file in .ssh directory with following content:
```
#Account1
Host gitlab.com
    HostName gitlab.com
    User git
    IdentityFile ~/.ssh/<private-key1-name>

#Account2 ("-second" - just an example)
Host gitlab.com-second  
    HostName gitlab.com
    User git
    IdentityFile ~/.ssh/<private-key2-name>>
```
2. Work with "Account1" as usual.

3. Work with "Account2":
```
# clone a repository
git clone git@gitlab.com-second:username>/repository.git
```
```
# Update existing work repo
git remote set-url origin git@gitlab.com-work:work/repository.git
```



